import React from 'react';
import MaterialTable from 'material-table';
import Container from "@material-ui/core/Container";
import fakeInstruments from "../fakedata/fakeInstruments"
import fakeContracts from "../fakedata/fakeContracts"


export default function MaterialTableDemo(props) {

    const [contracts] = React.useState(fakeContracts)
    const contractLookup = contracts.reduce((lookupMap, c) => {
        lookupMap[c.id] = c.name;
        return lookupMap;
    }, {});
    console.log(contractLookup);
    const [state, setState] = React.useState({
        columns: [
            { title: 'Contract Id', field: 'contractId', lookup: contractLookup},
            { title: 'Expiration', field: 'expirationDate', type: 'date' },
            { title: 'Activation', field: 'activationDate' },
            { title: 'Last Trading', field: 'lastTradingDate' },
            { title: 'Settlement', field: 'settlementDate' },
            { title: 'Status', field: 'statusId', type: 'numeric' },
            // {
            //     title: 'Birth Place',
            //     field: 'birthCity',
            //     lookup: { 34: 'İstanbul', 63: 'Şanlıurfa' },
            // },
        ],
        data: fakeInstruments,
    });

    return (
        <Container>
            <MaterialTable
                title="Editable Example"
                columns={state.columns}
                data={state.data}
                options={{
                    selection: false // <-- provided as props
                }}
                editable={{
                    onRowAdd: newData =>
                        new Promise(resolve => {
                            setTimeout(() => {
                                resolve();
                                const data = [...state.data];
                                data.push(newData);
                                setState({ ...state, data });
                            }, 600);
                        }),
                    onRowUpdate: (newData, oldData) =>
                        new Promise(resolve => {
                            setTimeout(() => {
                                resolve();
                                const data = [...state.data];
                                data[data.indexOf(oldData)] = newData;
                                setState({ ...state, data });
                            }, 600);
                        }),
                    onRowDelete: oldData =>
                        new Promise(resolve => {
                            setTimeout(() => {
                                resolve();
                                const data = [...state.data];
                                data.splice(data.indexOf(oldData), 1);
                                setState({ ...state, data });
                            }, 600);
                        }),
                }}
            />
        </Container>
    );
}