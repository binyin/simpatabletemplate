export default class Instrument {
    constructor(instrumentData) {
        this.contractId = instrumentData.contractId;
        this.expirationDate = instrumentData.expirationDate;
        this.activationDate = instrumentData.activationDate;
        this.lastTradingDate = instrumentData.lastTradingDate;
        this.settlementDate = instrumentData.settlementDate;
        this.statusId = instrumentData.statusId;
    }
}