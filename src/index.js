import React from 'react';
import ReactDOM from 'react-dom';
import StagedTable from './components/StagedTable'

ReactDOM.render(<StagedTable />, document.getElementById('root'));
