function generateContracts() {
    return [
        {id: 123456789, name: 'CCVA Bin Yin Contract'},
        {id: 123456790, name: 'CCTV AVH Contract'},
    ];
}

export default generateContracts();