function generateInstruments() {
    return [
        {
            expirationDate: '2019-02-01', activationDate: '2019-01-01', lastTradingDate: '2019-03-01',
            settlementDate: '2019-04-01', statusId: 1, contractId: 123456789
        },
        {
            expirationDate: '2019-03-01', activationDate: '2019-01-01', lastTradingDate: '2019-03-01',
            settlementDate: '2019-04-01', statusId: 1, contractId: 123456790
        },
        {
            expirationDate: '2019-02-01', activationDate: '2019-01-01', lastTradingDate: '2019-03-01',
            settlementDate: '2019-04-01', statusId: 1, contractId: 123456789
        },
        {
            expirationDate: '2019-03-01', activationDate: '2019-01-01', lastTradingDate: '2019-03-01',
            settlementDate: '2019-04-01', statusId: 1, contractId: 123456790
        },
        {
            expirationDate: '2019-02-01', activationDate: '2019-01-01', lastTradingDate: '2019-03-01',
            settlementDate: '2019-04-01', statusId: 1, contractId: 123456789
        },
        {
            expirationDate: '2019-03-01', activationDate: '2019-01-01', lastTradingDate: '2019-03-01',
            settlementDate: '2019-04-01', statusId: 1, contractId: 123456790
        }
    ];
}

export default generateInstruments();